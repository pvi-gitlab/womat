# womat

Wordpress Management Tool

This tool is mad to industrialize Wordpress sites installation and update

It is currently able to install & configure a Wordpress

There is still features to code :
  - Automatic Nginx configuration (If a good soul could code it for Apache it could be great)
  - Automatic Letsencrypt configuration
  - Parameters control
  - Automatic Wordpress Updates
  - Backup & Rollback

It can be used like this :

./womat install --dir=/home/user/www/sitedir --database=databasename --db_user=databaseusername --domain=fully.qualified.domain.name --aliases=other.domain.name,third.domain.name  --webserver=nginx --admin=adminusername --admin_email=admin.mail@domain.name --accounts=administrator:adminusername2:admin2.email@domain.name,administrator:adminusername3:admin3.email@domain.name,editor:editorname1:editor1.email@domain.name --profile=profilename --sys_user=systemusername --title="title of web site" --smtp_server="smtp.domain.name" --smtp_autentication="yes" --smtp_username="some.user@domain.name" --smtp_password="ANicePassword1234" --smtp_encrypt_pass="false" --smtp_port="25" --smtp_type_encryption="tls"

Il will do everything : make the dirs, create the database and its user, download wordpress, configure it, install it, download plugins, activate some of them, configure them (like smtp server), activate a theme, create users and give them their roles...
