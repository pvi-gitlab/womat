Introdutction
=============

Installing womat is pretty simple.

**1) Go to release page and download the last archive ( .zip or .tar.gz or even .tar.bz2 )**

    There -> https://gitlab.com/pvi-gitlab/womat/-/releases

    Note: on the release page is's presented as "source code" which is normal,
    because there is no need to build shellscripts to use them.

```bash
    # For example : 
    cd $HOME/Downloads
    wget https://gitlab.com/pvi-gitlab/womat/-/archive/v0.3/womat-v0.3.zip
```


**2) Unzip it somewhere in your personal home directory.**

    For this i recommend to first create a first level directory, for example "apps" or "tools".
    This way adding a lot of tools can be done without polluting your home directory

```bash
    # For example :
    cd $HOME
    mkdir apps
    cd apps
    unzip $HOME/Dowlnloads/womat-v0.3.zip
```

**3) run womat once.**

    It will perfmorm requirement checks and process to its own installation and initialization.

    If your $HOME dir have a $HOME/bin subdir womat will create there a soft link called "womat",
    allowing you to run it from everywhere.

```bash
    # For example :
    cd $HOME/apps/womat-v0.3
    chmod +x womat.sh
    ./womat.sh
```
    Docsign will tell you the list of missing requirements (pdftk or texlive-dist for example)

**3) Install the missing requirements.**

    With apt on debian like distributions, with yum or dnf for Red Hat like distributions, 
    with urpmi on Mageia, with yast on Open Suse like distributions...


**4) Now you can use womat.**

    You'll have first to add a signture and probably to create some setups to suit your needs...

    -> See README and other docs like user guides for that...

    Enjoy :)


